package com.zenghus.spring.demo;


import com.zenghus.spring.formework.annotation.Autowired;
import com.zenghus.spring.formework.annotation.Controller;
import com.zenghus.spring.formework.annotation.RequestMapping;
import com.zenghus.spring.formework.annotation.RequestParam;
import com.zenghus.spring.demo.service.DemoService;
import com.zenghus.spring.formework.webmvc.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zenghu
 * @date 2018/4/28 18:52
 */
@Controller
@RequestMapping("/demo")
public class DemoController {
    @Autowired
    DemoService demoService;

    @RequestMapping("/query.json")
    public void query(@RequestParam("name") String name,HttpServletResponse resp){
        String result=demoService.get(name);
        out(resp,result);
    }

    @RequestMapping("/frist")
    public ModelAndView frist(@RequestParam("name") String name,@RequestParam("addr") String addr){
        Map<String,Object> map=new HashMap<String, Object>();
        String result=demoService.get(name);
        map.put("name",result);
        map.put("addr",addr);
        ModelAndView modelAndView=new ModelAndView("first.html",map);
        return modelAndView;
    }

    private ModelAndView out(HttpServletResponse resp,String str){
        try {
            resp.getWriter().write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
