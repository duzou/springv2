package com.zenghus.spring.demo.aspect;

/**
 * @author zenghu
 * @date 2018/5/6 14:54
 */
public class LogAspect {

    //调用之前执行当前方法
    public void before(){
        System.out.println("method before......");
    }

    //调用之后执行当前方法
    public void after(){
        System.out.println("method after......");
    }
}
