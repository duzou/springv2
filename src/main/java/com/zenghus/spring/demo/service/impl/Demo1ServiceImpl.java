package com.zenghus.spring.demo.service.impl;

import com.zenghus.spring.demo.service.Demo1Service;
import com.zenghus.spring.demo.service.DemoService;
import com.zenghus.spring.formework.annotation.Autowired;
import com.zenghus.spring.formework.annotation.Service;

/**
 * @author zenghu
 * @date 2018/5/6 16:37
 */
@Service
public class Demo1ServiceImpl implements Demo1Service{
    @Autowired
    DemoService demoService;

    @Override
    public String getname(String name) {
        System.out.println("demo1ServiceImpl getname");
        return demoService.getBegin()+name;
    }
}
