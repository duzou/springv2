package com.zenghus.spring.demo.service.impl;


import com.zenghus.spring.demo.service.Demo1Service;
import com.zenghus.spring.formework.annotation.Autowired;
import com.zenghus.spring.formework.annotation.Service;
import com.zenghus.spring.demo.service.DemoService;

@Service
public class DemoServiceImpl implements DemoService {

	@Autowired
	Demo1Service demo1Service;

	public String get(String name) {
		// TODO Auto-generated method stub
		System.out.println("demoServiceImpl get");
		return "My name is "+demo1Service.getname(name);
	}

	@Override
	public String getBegin() {
		System.out.println("demoServiceImpl getBegin");
		return " demo begin ";
	}


}
