package com.zenghus.spring.formework.aop;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zenghu
 * @date 2018/5/6 14:57
 */
//目标代理对象的一个方法，要增强
//由用户自己实现的业务逻辑去增强
//配置文件的目的就是告诉spring，哪些类的哪些方法需要增强，增强的内容是什么
public class AopConfig {

    //把目标对象需要增强的method做为key，把需要增强的代码内容做完value
    private Map<Method,Aspect> aspects=new HashMap<Method, Aspect>();

    public void put(Method method,Object aspect,Method[] points){
        aspects.put(method,new Aspect(aspect,points));
    }

    public Aspect get(Method method) {
        return aspects.get(method);
    }

    public boolean contains(Method method){
        return this.aspects.containsKey(method);
    }

    //对增强的代码的封装
    public class Aspect{
        private Object aspect;//增强的对象
        private Method[] points;//增强的方法，before，after

        public Aspect(Object aspect, Method[] points) {
            this.aspect = aspect;
            this.points = points;
        }

        public Object getAspect() {
            return aspect;
        }

        public Method[] getPoints() {
            return points;
        }
    }

}
