package com.zenghus.spring.formework.bean;

/**
 * @author zenghu
 * @date 2018/5/1 12:13
 */
//存储配置文件中的信息
public class BeanDefinition {

    private String beanClassName;//class

    private String factoryBeanName;//在ioc中class的name

    private Boolean lazyinit=false;//是否是延迟加载

    public String getBeanClassName() {
        return beanClassName;
    }

    public void setBeanClassName(String beanClassName) {
        this.beanClassName = beanClassName;
    }

    public String getFactoryBeanName() {
        return factoryBeanName;
    }

    public void setFactoryBeanName(String factoryBeanName) {
        this.factoryBeanName = factoryBeanName;
    }

    public Boolean getLazyinit() {
        return lazyinit;
    }

    public void setLazyinit(Boolean lazyinit) {
        this.lazyinit = lazyinit;
    }
}
