package com.zenghus.spring.formework.bean;

/**
 * @author zenghu
 * @date 2018/5/1 13:25
 */

//用作事件监听
public class BeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean,String beanName){
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean,String beanName){
        return bean;
    }
}
