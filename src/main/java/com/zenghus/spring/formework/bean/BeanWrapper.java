package com.zenghus.spring.formework.bean;

import com.zenghus.spring.formework.aop.AopConfig;
import com.zenghus.spring.formework.aop.AopProxy;
import com.zenghus.spring.formework.core.FactoryBean;

/**
 * @author zenghu
 * @date 2018/5/1 12:13
 */
public class BeanWrapper extends FactoryBean {

    private AopProxy aopProxy=new AopProxy();

    //还会用到观察者模式
    //1.支持事件响应，会用监听
    private BeanPostProcessor beanPostProcessor;

    private Object wrapperInstance;//封装后的对象
    private Object originalInstance;//原始对象（通过反射new出来的）

    public BeanWrapper(Object instance){
        this.wrapperInstance=aopProxy.getProxy(instance);
        this.originalInstance=instance;
    }

    public Object getWrapperInstance(){
        return wrapperInstance;
    }

    public BeanPostProcessor getBeanPostProcessor() {
        return beanPostProcessor;
    }

    public void setBeanPostProcessor(BeanPostProcessor beanPostProcessor) {
        this.beanPostProcessor = beanPostProcessor;
    }

    public void setWrapperInstance(Object wrapperInstance) {
        this.wrapperInstance = wrapperInstance;
    }

    public Object getOriginalInstance() {
        return originalInstance;
    }

    public void setOriginalInstance(Object originalInstance) {
        this.originalInstance = originalInstance;
    }

    //返回代理后的class
    public Class<?> getWrappedClass(){
        return this.wrapperInstance.getClass();
    }

    public void setAopConfig(AopConfig aopConfig){
        aopProxy.setConfig(aopConfig);
    }
}
