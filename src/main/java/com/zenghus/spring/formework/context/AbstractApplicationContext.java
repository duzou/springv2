package com.zenghus.spring.formework.context;

/**
 * @author zenghu
 * @date 2018/5/6 13:58
 */
public abstract class AbstractApplicationContext {

    /**
     * 提供给子类重写的
     */
    protected  void onRefresh(){

    }


    protected abstract void refreshBeanFactory();


}
