package com.zenghus.spring.formework.context;

/**
 * @author zenghu
 * @date 2018/5/6 14:04
 */
public interface ApplicationContextAware {

    void setApplicationContext(ApplicationContext applicationContext);
}
