package com.zenghus.spring.formework.context;

import com.zenghus.spring.formework.bean.BeanDefinition;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zenghu
 * @date 2018/5/6 14:02
 */
public class DefaultListableBeanFactory extends AbstractApplicationContext {

    //用来保存配置形象
    protected Map<String,BeanDefinition> beanDefinitionMap=new ConcurrentHashMap<String, BeanDefinition>();

    @Override
    protected void onRefresh() {
        super.onRefresh();
    }

    @Override
    protected void refreshBeanFactory() {

    }
}
