package com.zenghus.spring.formework.core;

/**
 * @author zenghu
 * @date 2018/5/1 12:08
 */
public interface BeanFactory {

    //根据beanname返回当前的实例
    Object getBean(String beanName);

}
